import React, { Fragment } from 'react';
import {
    Backdrop, CircularProgress, List, ListItem, ListItemText,
    Box, Fab, Card, Divider, CardMedia, Collapse, Snackbar, SnackbarContent, IconButton,
    Dialog, DialogTitle, DialogContent, DialogContentText, TextField, DialogActions, Button
} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import CloseIcon from '@material-ui/icons/Close';

const renderRow = (data, key) => {
    return data.map((item) => (
        <Fragment>
            <ListItem dense>
                <ListItemText primary={item[key].name} />
            </ListItem>
            <Divider />
        </Fragment>
    ))
}

const PokemonDetailComponent = ({ onChangeText, isSuccess, onSave, onRelease, isLoading, image, name, type, ability, isAbilitiesExpand, move, isMovesExpand, onExpand, closeSnackBar, isSnackBarShow, onCatchPokemon }) => {
    return (
        <Fragment>
            <Dialog open={isSuccess}>
                <DialogTitle>Success</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        {`${name} was captured. Give a nickname`}
                    </DialogContentText>
                    <TextField
                        autoFocus
                        margin="dense"
                        label="NickName"
                        fullWidth
                        onChange={(e)=> {onChangeText(e.target.value)}}
                    />
                </DialogContent>
                <DialogActions>
                    <Button color="primary" onClick={() => { onRelease() }}>
                        Release
                    </Button>
                    <Button color="primary" onClick={() => { onSave() }}>
                        Save
                    </Button>
                </DialogActions>
            </Dialog>
            <Backdrop
                open={isLoading}
            >
                <CircularProgress color="inherit" />
            </Backdrop>
            <Box style={{ width: '100%', textAlign: '-webkit-center' }}>
                <Card style={{ borderRadius: 150, width: 150, backgroundColor: 'grey' }}>
                    <CardMedia
                        style={{ height: 150, width: 150 }}
                        image={image}
                    />
                </Card>
                <h1>{name}</h1>
                <p>{type}</p>
                <ListItem button onClick={() => onExpand('isAbilitiesExpand')}>
                    <ListItemText primary={`Abilities (${ability.length})`} />
                    {isAbilitiesExpand ? <ExpandLess /> : <ExpandMore />}
                </ListItem>
                <Collapse in={isAbilitiesExpand} timeout="auto" unmountOnExit>
                    <List>
                        {renderRow(ability, 'ability')}
                    </List>
                </Collapse>

                <ListItem button onClick={() => onExpand('isMovesExpand')}>
                    <ListItemText primary={`Moves (${move.length})`} />
                    {isMovesExpand ? <ExpandLess /> : <ExpandMore />}
                </ListItem>
                <Collapse in={isMovesExpand} timeout="auto" unmountOnExit>
                    <List>
                        {renderRow(move, 'move')}
                    </List>
                </Collapse>
                <Snackbar
                    variant="error"
                    anchorOrigin={{
                        vertical: 'top',
                        horizontal: 'right',
                    }}
                    open={isSnackBarShow}
                    autoHideDuration={1000}
                    onClose={() => closeSnackBar()}
                >
                    <SnackbarContent
                        style={{ backgroundColor: '#d32f2f' }}
                        aria-describedby="client-snackbar"
                        message={`oops... ${name} has run away`}
                        action={[
                            <IconButton key="close" aria-label="close" color="inherit" onClick={() => closeSnackBar()}>
                                <CloseIcon />
                            </IconButton>,
                        ]}
                    />
                </Snackbar>
                <Fab
                    onClick={() => onCatchPokemon()}
                    color="secondary"
                    style={{ position: 'fixed', right: 20, bottom: 20 }}
                >
                    <AddIcon />
                </Fab>
            </Box>
        </Fragment>
    )
}

export default PokemonDetailComponent;