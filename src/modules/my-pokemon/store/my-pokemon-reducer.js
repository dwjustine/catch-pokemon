import * as actionType from './my-pokemon-action-type';

const initialState = {
    ownedPokemon: [],
}

export default (state = initialState, action) => {
    const { payload, type } = action;
    const newData = [...state.ownedPokemon]
    switch (type) {
        case actionType.ADD_POKEMON:
            if (state.ownedPokemon.length !== 0) {
                const index = state.ownedPokemon.findIndex(x => x.name === payload.name)
                if (index === -1) {
                    newData.push({ name: payload.name, data: [payload.nickName] })
                } else {
                    newData[index].data.push(payload.nickName)
                }
            } else {
                newData.push({ name: payload.name, data: [payload.nickName] })
            }
            return { ...state, ownedPokemon: [...newData] };
        case actionType.REMOVE_POKEMON:
            if (newData[payload.nameIndex].data.length === 1) {
                newData.splice(payload.nameIndex, 1)
            } else {
                newData[payload.nameIndex].data.splice(payload.nickNameIndex, 1)
            }
            return { ...state, ownedPokemon: [...newData] };
        default:
            return state;
    }
}