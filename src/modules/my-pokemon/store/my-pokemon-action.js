import * as actionType from './my-pokemon-action-type';

const addPokemon = (payload) => ({
    type: actionType.ADD_POKEMON,
    payload
})

const removePokemon = (payload) => ({
    type: actionType.REMOVE_POKEMON,
    payload
})

export const savePokemon = (name, nickName) => (dispatch) => {
    dispatch(addPokemon({ name, nickName }));
};

export const releasePokemon = (nameIndex, nickNameIndex) => (dispatch) => {
    dispatch(removePokemon({ nameIndex, nickNameIndex }));
};