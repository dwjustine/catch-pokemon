import React, { Fragment } from 'react';
import { ExpansionPanel, ExpansionPanelSummary, ExpansionPanelDetails, Typography, List, ListItem, ListItemText, IconButton } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import DeleteIcon from '@material-ui/icons/Delete';


const renderChild = (nameIndex, data, onReleasePokemon) => {
    return data.map((item, index) => (
        <ListItem dense>
            <ListItemText primary={item} />
            <IconButton
                size='small'
                onClick={() => { onReleasePokemon(nameIndex, index) }}
            >
                <DeleteIcon />
            </IconButton>
        </ListItem>
    ))
}

const renderParent = (data, onReleasePokemon) => {
    if (data.length !== 0) {
        return data.map((item, index) => (
            <ExpansionPanel>
                <ExpansionPanelSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel1a-content"
                    id="panel1a-header"
                >
                    <Typography >{item.name}</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <List style={{ width: '100%' }}>
                        {renderChild(index, item.data, onReleasePokemon)}
                    </List>
                </ExpansionPanelDetails>
            </ExpansionPanel >
        ))
    }
    return <Typography >You haven't caught any pokemon</Typography>
}

const PokemonDetailComponent = ({ data, onReleasePokemon }) => {
    return (
        <Fragment>
            {renderParent(data, onReleasePokemon)}
        </Fragment>
    )
}

export default PokemonDetailComponent;