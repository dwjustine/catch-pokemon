import React, { Fragment } from 'react';
import { List, ListItem, ListItemText, Button, CircularProgress, Box, Divider } from '@material-ui/core';

const renderRow = (data, navigate) => {
    return data.map((item) => (
        <Fragment>
            <ListItem dense button onClick={() => { navigate(`/pokemon-detail/${item.name}`) }}>
                <ListItemText primary={item.name} />
                <ListItemText secondary={item.count} secondaryTypographyProps={{ align: 'right' }} />
            </ListItem>
            <Divider />
        </Fragment>
    ))
}

const PokemonListComponent = ({ isLoading, data, url, onLoadMore, navigate }) => {
    return (
        <Fragment>
            <List>
                {renderRow(data, navigate)}
            </List>
            {isLoading ?
                <Box style={{ width: '100%', textAlign: 'center' }}>
                    <CircularProgress size={30} thickness={5} />
                </Box> :
                url &&
                <Button variant="outlined" fullWidth onClick={() => onLoadMore(url, false)} >
                    Load More
                </Button>
            }
        </Fragment>
    )
}

export default PokemonListComponent;