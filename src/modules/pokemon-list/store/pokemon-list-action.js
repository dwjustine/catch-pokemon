import * as actionType from './pokemon-list-action-type';
import Axios from 'axios';

const setListPokemon = (payload) => ({
    type: actionType.SET_LIST_POKEMON,
    payload
})

export const getList = (url, data, clearList) => (dispatch) => {
    return new Promise((resolve, reject) => {
        Axios.get(url)
            .then((res) => {
                const payload = { clearList, list: [...res.data.results], existed: data }
                dispatch(setListPokemon(payload));
                resolve(res);
            })
    });
};