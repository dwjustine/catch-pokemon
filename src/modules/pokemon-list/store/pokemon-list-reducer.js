import * as actionType from './pokemon-list-action-type';
import { BASE_URL } from './../../../common/properties';

const initialState = {
    data: [],
    url: BASE_URL,
}

export default (state = initialState, action) => {
    const { payload, type } = action;
    const newData = [];
    switch (type) {
        case actionType.SET_LIST_POKEMON:
            payload.list.map((item) => {
                const index = payload.existed.findIndex(x => x.name === item.name)
                const count = index === -1 ? 0 : payload.existed[index].data.length
                newData.push({ name: item.name, count })
                return null;
            })
            const data = payload.clearList ? [...newData] : [...state.data, ...newData];
            return { ...state, data };
        default:
            return state;
    }
}