import React from "react";
import { HashRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { store, persistor } from './common/store';
import Layout from './common/layout'

export default function App() {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <HashRouter>
          <Layout />
        </HashRouter>
      </PersistGate>
    </Provider>
  );
}