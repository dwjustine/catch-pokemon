import React, { Component } from 'react';
import { connect } from 'react-redux';
import { releasePokemon } from '../../modules/my-pokemon/store/my-pokemon-action'
import MyPokemonComponent from '../../modules/my-pokemon/component/my-pokemon-component';

class MyPokemon extends Component {
    onReleasePokemon = (nameIndex, nickNameIndex) => {
        this.props.releasePokemon(nameIndex, nickNameIndex)
    }

    render() {
        return (
            <MyPokemonComponent
                data={this.props.myPokemon.ownedPokemon}
                onReleasePokemon={this.onReleasePokemon}
            />
        )
    }
}

const mapStateToProps = (state) => {
    return {
        myPokemon: { ...state.MyPokemonReducer }
    }
}

const mapActionToProps = () => {
    return {
        releasePokemon
    }
}

export default connect(mapStateToProps, mapActionToProps())(MyPokemon);