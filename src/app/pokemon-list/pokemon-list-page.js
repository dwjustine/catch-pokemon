import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { getList } from './../../modules/pokemon-list/store/pokemon-list-action';
import { BASE_URL } from '../../common/properties';
import PokemonListComponent from './../../modules/pokemon-list/component/pokemon-list-component';

class PokemonList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            url: '',
            isLoading: false,
        }
    }

    componentDidMount() {
        this.getPokemonList(BASE_URL, true);
    }

    getPokemonList = (url, clearList) => {
        this.setState({ isLoading: true })
        this.props.getList(url, this.props.myPokemon.ownedPokemon, clearList)
        .then((res) => this.setState({
            url: res.data.next,
            isLoading: false,
        }))
    }

    render() {
        return (
            <PokemonListComponent
                isLoading={this.state.isLoading}
                data={this.props.pokemonList.data}
                url={this.state.url}
                onLoadMore={this.getPokemonList}
                navigate={this.props.history.push}
            />
        )
    }
}

const mapStateToProps = (state) => {
    return {
        myPokemon: { ...state.MyPokemonReducer },
        pokemonList: { ...state.PokemonListReducer }
    }
}

const mapActionToProps = () => {
    return {
        getList,
    }
}

export default connect(mapStateToProps, mapActionToProps())(withRouter(PokemonList));