import React, { Component } from 'react';
import Axios from 'axios';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { BASE_URL } from '../../common/properties';
import PokemonDetailComponent from '../../modules/pokemon-detail/component/pokemon-detail-component';
import { savePokemon } from '../../modules/my-pokemon/store/my-pokemon-action';

class PokemonDetail extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isSuccess: false,
            url: `${BASE_URL}/${props.match.params.name}`,
            isLoading: false,
            name: props.match.params.name,
            image: '',
            type: '',
            ability: [],
            isAbilitiesExpand: false,
            move: [],
            isMovesExpand: false,
            nickName: '',
        }
    }

    componentDidMount() {
        this.getPokemonDetail();
    }

    getPokemonDetail = () => {
        this.setState({ isLoading: true })
        Axios.get(this.state.url)
            .then((res) => {
                let type = '';
                res.data.types.map(element => (type = `${type}${element.type.name} / `))
                this.setState({
                    isLoading: false,
                    image: res.data.sprites.front_default,
                    type: type.slice(0, -3),
                    ability: res.data.abilities,
                    move: res.data.moves
                })
            })
    }

    onExpand = (keyState) => {
        this.setState({ [keyState]: !this.state[keyState] })
    }

    closeSnackBar = () => {
        this.setState({
            isSnackBarShow: false,
        })
    }

    onCatchPokemon = () => {
        const success = Math.round(Math.random()) === 1 ? true : false;
        if (success) {
            this.setState({
                isSnackBarShow: false,
                isSuccess: true,
            })
        } else {
            this.setState({
                isSnackBarShow: true,
                isSuccess: false,
            })
        }
    }

    onRelease = () => {
        this.setState({
            isSuccess: false,
            nickName: ''
        })
    }

    onSave = () => {
        this.props.savePokemon(this.state.name, this.state.nickName)
        this.setState({
            isSuccess: false,
            nickName: ''
        })
    }

    onChangeText = (value) => {
        this.setState({
            nickName: value,
        })
    }

    render() {
        return (
            <PokemonDetailComponent
                onChangeText={this.onChangeText}
                isSuccess={this.state.isSuccess}
                onRelease={this.onRelease}
                onSave={this.onSave}
                isLoading={this.state.isLoading}
                image={this.state.image}
                name={this.state.name}
                type={this.state.type}
                ability={this.state.ability}
                isAbilitiesExpand={this.state.isAbilitiesExpand}
                move={this.state.move}
                isMovesExpand={this.state.isMovesExpand}
                onExpand={this.onExpand}
                closeSnackBar={this.closeSnackBar}
                isSnackBarShow={this.state.isSnackBarShow}
                onCatchPokemon={this.onCatchPokemon}
            />
        )
    }
}

const mapStateToProps = (state) => {
    return {
        myPokemon: { ...state.MyPokemonReducer },
    }
}

const mapActionToProps = () => {
    return {
        savePokemon,
    }
}

export default connect(mapStateToProps, mapActionToProps())(withRouter(PokemonDetail));