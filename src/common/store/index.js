import { createStore, combineReducers, applyMiddleware } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import thunk from 'redux-thunk';
import MyPokemonReducer from './../../modules/my-pokemon/store/my-pokemon-reducer';
import PokemonListReducer from '../../modules/pokemon-list/store/pokemon-list-reducer';

const persistConfig = {
    key: 'catch-pokemon',
    storage,
    whitelist: ['MyPokemonReducer'],
};

const reducers = combineReducers({
    MyPokemonReducer,
    PokemonListReducer
});

const persistedReducer = persistReducer(persistConfig, reducers);

const store = createStore(persistedReducer, applyMiddleware(thunk));

const persistor = persistStore(store);

export { store, persistor };