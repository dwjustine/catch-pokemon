import React, { Component } from 'react';

class NotFoundPage extends Component {
    render(){
        return(
            <h1>403 Not Found</h1>
        )
    }
}

export default NotFoundPage;