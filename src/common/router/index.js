import React from 'react';
import { Switch, Route } from "react-router-dom";
import routerConfig from './router-config';
import NotFoundPage from './../general-component/403/403-page'

const route = () => {
  const routes = routerConfig.map((item, index) => {
    return (
      <Route exact path={item.path}>
        <item.component />
      </Route>
    )
  })
  routes.push(
    <Route>
      <NotFoundPage />
    </Route>
  )
  return routes;
}

const router = () => {
  return (
    <Switch>
      {route()}
    </Switch>
  );
}

export default router;