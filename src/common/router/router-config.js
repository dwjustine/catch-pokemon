import PokemonListPage from './../../app/pokemon-list/pokemon-list-page';
import PokemonDetailPage from './../../app/pokemon-detail/pokemon-detail-page';
import MyPokemon from './../../app/my-pokemon/my-pokemon-page';


const routerConfig = [
    {
        component: PokemonListPage,
        path: '/'
    },
    {
        component: PokemonDetailPage,
        path: '/pokemon-detail/:name'
    },
    {
        component: MyPokemon,
        path: '/my-pokemon'
    }
]

export default routerConfig;